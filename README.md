#Dota2 Katkastatsbot for Telegram users

25/02/2019

This bot was created with the help of Opendota API + pyTelegramBotAPI. 
The bot understands commands /statsdota 'Name' and /lastgames (num) 'Name'.
Player names can contain spaces. 
Lastgames defaults to 1, the max number of fetched games is 5.
Stats command fetches a hero's current winrate in Immortal bracket.


Example commands:


/statsdota Example Player

/lastgames 3 Example Player

/stats Axe

-----------------------------------------------------------------------------
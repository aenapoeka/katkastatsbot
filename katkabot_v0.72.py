# coding=utf-8
# Tämä bot pystyy etsimään Opendota API:n avulla halutun pelaajan Dota2-pelituloksia.
# Kehitysversiossa on mahdollista etsiä halutun pelaajan tulosyhteenveto ja viime pelit.

import telebot
import time
import requests
import random


bot_token ='TOKEN'

bot = telebot.TeleBot(bot_token)


# Testikomennot

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, 'Welcome!')

@bot.message_handler(commands=['help'])
def send_help(message):
    bot.reply_to(message, 'To start searching player data: \nSend me command \'/statsdota playerName\'\nOther available commands are:'
    +'\n/stats heroName, \n/lastgames number playerName, \n/roll')

@bot.message_handler(commands=['batrider'])
def batraada(message):
    bot.reply_to(message, 'https://www.opendota.com/matches/59822764')

@bot.message_handler(commands=['kuningastuali'])
def kuningas(message):
    bot.reply_to(message, 'https://www.opendota.com/matches/1247794262')

@bot.message_handler(commands=['alchemistcamp'])
def alch(message):
    bot.reply_to(message, 'https://www.opendota.com/matches/1235301889')

@bot.message_handler(commands=['sinnemeni'])
def razor(message):
    bot.reply_to(message, 'https://www.opendota.com/matches/2671521661')

@bot.message_handler(commands=['roll'])
def roll(message):
    bot.reply_to(message, round(random.random() * 100))


# Main

# /lastgames-komento hakee halutun pelaajan viimeiset 1-5 peliä ja lähettää ne viesteinä hakijalle.
# Komennossa on vaihtoehtona valita itse haettavien pelien määrä(1-5) tai käyttää oletusmäärää pelejä(1).
@bot.message_handler(commands=['lastgames'], func=lambda msg: msg.text is not None)
def games(message):
    parts = message.text.split()
    partsLength = len(parts)
    keyword = ""
    playerNumber = 0
    playerName = ""
    heroDict = {"1":"Anti-Mage", "2":"Axe", "3":"Bane", "4":"Bloodseeker", "5":"Crystal Maiden",
    "6":"Drow Ranger", "7":"Earthshaker", "8":"Juggernaut", "9":"Mirana", "10":"Morphling",
    "11":"Shadow Fiend", "12":"Phantom Lancer", "13":"Puck", "14":"Pudge", "15":"Razor",
    "16":"Sand King", "17":"Storm Spirit", "18":"Sven", "19":"Tiny", "20":"Vengeful Spirit",
    "21":"Windrunner", "22":"Zeus", "23":"Kunkka", "25":"Lina", "26":"Lion", "27":"Shadow Shaman",
    "28":"Slardar", "29":"Tidehunter", "30":"Witch Doctor", "31":"Lich", "32":"Riki", "33":"Enigma",
    "34":"Tinker", "35":"Sniper", "36":"Necrophos", "37":"Warlock", "38":"Beastmaster",
    "39":"Queen of Pain", "40":"Venomancer", "41":"Faceless Void", "42":"Wraith King",
    "43":"Death Prophet", "44":"Phantom Assassin", "45":"Pugna", "46":"Templar Assassin",
    "47":"Viper", "48":"Luna", "49":"Dragon Knight", "50":"Dazzle", "51":"Clockwerk",
    "52":"Leshrac", "53":"Nature's Prophet", "54":"Lifestealer", "55":"Dark Seer", "56":"Clinkz",
    "57":"Omniknight", "58":"Enchantress", "59":"Huskar", "60":"Nightstalker", "61":"Broodmother",
    "62":"Bounty Hunter", "63":"Weaver", "64":"Jakiro", "65":"Batrider", "66":"Chen", "67":"Spectre",
    "68":"Ancient Apparition", "69":"Doom", "70":"Ursa", "71":"Spirit Breaker", "72":"Gyrocopter",
    "73":"Alchemist", "74":"Invoker", "75":"Silencer", "76":"Outworld Devourer", "77":"Lycan",
    "78":"Brewmaster", "79":"Shadow Demon", "80":"Lone Druid", "81":"Chaos Knight", "82":"Meepo",
    "83":"Treant Protector", "84":"Ogre Magi", "85":"Undying", "86":"Rubick", "87":"Disruptor",
    "88":"Nyx Assassin", "89":"Naga Siren", "90":"Keeper of The Light", "91":"Io", "92":"Visage",
    "93":"Slark", "94":"Medusa", "95":"Troll Warlord", "96":"Centaur Warrunner", "97":"Magnus",
    "98":"Timbersaw", "99":"Bristleback", "100":"Tusk", "101":"Skywrath Mage", "102":"Abaddon",
    "103":"Elder Titan", "104":"Legion Commander", "105":"Techies", "106":"Ember Spirit",
    "107":"Earth Spirit", "108":"Underlord", "109":"Terrorblade", "110":"Phoenix", "111":"Oracle",
    "112":"Winter Wyvern", "113":"Arc Wardern", "114":"Monkey King", "119":"Dark Willow",
    "120":"Pangolier", "121":"Grimstroke"}

    gameModes = {"1":"All Pick", "2":"Captains Mode", "3":"Random Draft", "4":"Single Draft",
    "5":"All Random", "6":"Least Played", "7":"Limited Heroes", "8":"Captains Draft",
    "9":"Ability Draft", "10":"Event", "11":"1v1 Solo Mid", "22":"All Draft", "23":"Turbo"}

    # Jos syöte ei ala komennolla
    if parts[0] != '/lastgames':
        bot.reply_to(message, "{}".format("Please use the following format: '/lastgames gameAmount(optional) playerName'"))

    # Komento annettu oikein, siinä on kaksi tai useampi osaa.
    # Pilkotun hakusyötteen ensimmäinen osa on komento, toinen pelien määrä ja loput varattu haettavalle nimelle.
    else:

        # Kaksiosaisessa komennossa on oltava itse komento ja pelaajan nimi. Pelien määräksi asetetaan 1.
        # Esimerkiksi "/lastgames pelaajaYksi"
        if partsLength == 2:
            keyword = str(parts[1])
            game_amount = 1

        # Jos komennossa on useampia osia, on selvitettävä onko toinen hakuparametri pelien määrälle.
        # Esimerkiksi "/lastgames 2 pelaajaYksi" TAI "/lastgames pelaaja yksi"
        if partsLength > 2:
            try:
                # Testataan onko komento muotoa "/lastgames 1 pelaajaYksi"
                # Asetetaan haettavien pelien määräksi haun 2. parametri
                game_amount = int(parts[1])
                if game_amount > 5:
                    game_amount = 5

                # Muodostetaan haettava pelaajan nimi haun lopuista parametreista
                keyword = parts[2]
                for nameParts in range(2, partsLength - 1):
                    keyword += " " + str(parts[nameParts+1])

            except ValueError as e:
                # Annettun moniosaisen haun hakuparametrit eivät sisältäneet numeroa.
                # Haku muotoa "/lastgames Pelaaja Yksi Kaksi", käytetään oletusmäärää peleille.
                print("Lastgames error:",e)
                print("Moniosaisen haun 2. parametri ei ollut numero, asetetaan pelien määräksi 1.")
                game_amount = 1
                keyword = parts[1]
                for nameParts in range(1, partsLength - 1):
                    keyword += " " + str(parts[nameParts+1])


        # Tulostetaan käyttäjälle millä hakusanalla botti aktivoitui
        bot.reply_to(message, "{}".format("\nHaetaan pelaajan \'" + str(keyword) + "\' viime pelit..."))


        # Hakutulokset
        searchParameters = {'q': keyword}
        search_response = requests.get('https://api.opendota.com/api/search', params=searchParameters).json()

        try:
            for g in range(len(search_response)-1):
                playerName = search_response[g]['personaname']

                # Vertaa onko jokin hakutuloksista sama kun haettu nimi
                if str(playerName).lower() == str(keyword).lower():
                    playerNumber = search_response[g]['account_id']

            matches_params = {'limit':20}
            profile_url = 'https://api.opendota.com/api/players/' + str(playerNumber)
            match_url = 'https://api.opendota.com/api/players/' + str(playerNumber) + '/matches'

            profileResponse = requests.get(profile_url).json()
            matchesResponse = requests.get(match_url, params=matches_params).json()

            # Haettujen pelien pelaaja-objekti
            class Player:

                def __init__(self, name):
                    self.name = name

                def __str__(self):
                    return " ".join(map(str, (self.name)))

            player = Player(profileResponse['profile']['personaname'])

            class Matches:

                matches = {}
                j = 0
                g = 0
                matchType = ""
                gameMode = ""
                hero = ""

                # Etsii pelit tuloksista ja listaa ne kirjastoksi.
                for match in matchesResponse:
                    matchData = match['match_id'], match['player_slot'], match['radiant_win'], match['hero_id'], match['lobby_type'], match['game_mode']
                    matches['new_match '+ str(g)] = matchData
                    g += 1

                # Ettii voittiko pelaaja.
                for match in matches:

                    # Tutkii mikä HERO pelaaja oli.
                    hero = heroDict[str(matches[match][3])]
                    print(hero)

                    # Tutkii mikä moodi ja minkä tyyppinen LOBBY oli.

                    if True:

                        # RANKED / UNRANKED

                        if int(matches[match][4] == 0):
                            matchType = "Unranked"
                        if int(matches[match][4] == 7):
                            matchType = "Ranked"

                        # GAME MODE

                        gameMode = gameModes[str(matches[match][5])]


                    matchResult = list()

                    matchResult.append("\nTulokset pelille " + str(matches[match][0]) +
                    ":\nPelin tyyppi: " + matchType + "\nPelimoodi: " + gameMode + "\n")

                    if int(matches[match][1]) < 128:
                        matchResult.append(player.name + " oli Radiant " + hero + ".")
                    else:
                        matchResult.append(player.name + " oli Dire " + hero + ".")

                    if int(matches[match][1] < 128 and matches[match][2] == True):
                        matchResult.append("\nPelaaja voitti.")
                    if int(matches[match][1] > 127 and matches[match][2] == False):
                        matchResult.append("\nPelaaja voitti.")
                    if int(matches[match][1] < 128 and matches[match][2] == False):
                        matchResult.append("\nPelaaja hävisi.")
                    if int(matches[match][1] > 127 and matches[match][2] == True):
                        matchResult.append("\nPelaaja hävisi.")

                    # game_amount on parametrina annettu lukumäärä, esim /lastgames 5 meinaa että h = 5
                    if j < game_amount:
                        bot.reply_to(message, "{}{}{}".format("\n".join(matchResult),
                        "\n_____________________\n",
                        "\nhttps://www.opendota.com/matches/" + str(matches[match][0])))
                    j += 1

        except Exception as e:
            print(e)
            bot.reply_to(message, "{}".format("Ei viimeaikaisia pelejä pelaajalle " + keyword + "."))


@bot.message_handler(commands=['statsdota'], func=lambda msg: msg.text is not None)
def katka(message):
    parts = message.text.split()
    try:
        partsLength = len(parts)
        keyword = ""
        playerNumber = 0
        playerName = ""

        # Jos ei kahta osaa komennos:
        if parts[0] != '/statsdota':
            bot.reply_to(message, "{}".format("Please use the following format: '/statsdota playerName'"))

        # Komento annettu oikein
        else:
            keyword = str(parts[1])
            if partsLength > 2:
                for q in range(1,partsLength - 1):
                    keyword += " " + str(parts[q+1])

            # Ilmoittaa käyttäjälle tämänhetkisen haun tilan
            #bot.reply_to(message, "{}{}".format("Haetaan tulokset pelaajalle:  \'", keyword + "\'."))

            # Hakutulokset
            searchParameters = {'q': keyword}
            search_response = requests.get('https://api.opendota.com/api/search', params=searchParameters).json()

            try:
                for g in range(len(search_response)-1):
                    playerName = search_response[g]['personaname']

                    # Vertaa onko joku hakutuloksista tismalleen sama kun haettu nimi
                    if str(playerName).lower() == str(keyword).lower():
                        playerNumber = search_response[g]['account_id']

                # Printtaa haetun pelaajan nimen ja pelaajanumeron
                # bot.reply_to(message, "{}{}".format("Pelaajan " + keyword + " pelaajanumero: ", playerNumber))

                matches_searchParameters = {'limit':20}
                profile_url = 'https://api.opendota.com/api/players/' + str(playerNumber)
                match_url = 'https://api.opendota.com/api/players/' + str(playerNumber) + '/matches'
                winrate_url = 'https://api.opendota.com/api/players/' + str(playerNumber) + '/wl'

                # print(profile_url, match_url, winrate_url)

                profileResponse = requests.get(profile_url).json()
                matchesResponse = requests.get(match_url, params=matches_searchParameters).json()
                winrateResponse = requests.get(winrate_url).json()

                # Näitä käytetään voitonlaskuloopis.
                class Player:

                    # Näihin tuloo rankkien nimet ja numerot
                    rank_name = ""
                    rank_number = 0
                    rank = 0
                    wins_alltime = 0
                    losses_alltime = 0

                    def __init__(self, name, rank, lb_rank, mmr):
                        self.name = name
                        self.rank = rank
                        self.lb_rank = lb_rank
                        self.mmr = mmr

                    def __str__(self):
                        return " ".join(map(str, (self.name, self.rank, self.lb_rank, self.mmr)))

                    #Rankinnimiämislooppi
                    @classmethod
                    def rank_namer(cls, rank, lb_rank):
                        cls.rank = rank
                        cls.lb_rank = lb_rank

                        if cls.rank != 0:

                            if cls.rank in range(11, 16):
                                cls.rank_name = "Herald"
                            if cls.rank in range(21, 26):
                                cls.rank_name = "Guardian"
                            if cls.rank in range(31, 36):
                                cls.rank_name = "Crusader"
                            if cls.rank in range(41, 46):
                                cls.rank_name = "Archon"
                            if cls.rank in range(51, 56):
                                cls.rank_name = "Legend"
                            if cls.rank in range(61, 66):
                                cls.rank_name = "Ancient"
                            if cls.rank in range(71, 76):
                                cls.rank_name = "Divine"
                            if cls.rank == 80:
                                cls.rank_name = "Immortal"
                            if cls.rank not in range(11,81):
                                cls.rank_name = "Uncalibrated"
                                cls.rank_number = ""

                            cls.rank = str(rank)
                            if cls.rank != 80 and cls.rank[1] == "1":
                                cls.rank_number = 1
                            if cls.rank[1] == "2":
                                cls.rank_number = 2
                            if cls.rank[1] == "3":
                                cls.rank_number = 3
                            if cls.rank[1] == "4":
                                cls.rank_number = 4
                            if cls.rank[1] == "5":
                                cls.rank_number = 5

                            if rank != None:
                                cls.rank = int(rank)
                                cls.rank_number = " " + str(cls.rank_number)
                            else:
                                pass

                            if cls.rank == 80:
                                cls.rank_number = " " + str(cls.lb_rank)

                    # Laskoo parametreina saatujen voittojen ja häviöören winraten. Palauttaa winraten.
                    @classmethod
                    def winrate_calc(cls, wins, losses):

                        cls.wins_alltime = wins
                        cls.losses_alltime = losses

                        cls.winrate = round(cls.wins_alltime/(cls.wins_alltime + cls.losses_alltime),3)*100

                        # Pitää olla että Yhteenveto saa winraten printattua.
                        return cls.winrate

                player = Player(profileResponse['profile']['personaname'], Player.rank_namer(profileResponse['rank_tier'],profileResponse['leaderboard_rank']),
                        profileResponse['leaderboard_rank'], profileResponse['mmr_estimate']['estimate'])

                class Matches:

                    h = 0
                    matches = {}
                    wins = 0
                    g = 0

                    # Ettii pelit tuloksista ja listaa ne kirjastoksi.
                    for match in matchesResponse:
                        lista = match['match_id'], match['player_slot'], match['radiant_win'], match['hero_id'], match['lobby_type'], match['game_mode']
                        matches['new_match '+ str(g)] = lista
                        g += 1

                    # Ettii voittiko pelaaja.
                    for match in matches:

                        if int(matches[match][1] < 128 and matches[match][2] == True):
                            wins += 1
                        if int(matches[match][1] > 127 and matches[match][2] == False):
                            wins += 1

                        current_winrate = round(wins/20,3)*100

                # Yhteenveto
                summary = list()


                summary.append("\nPelaajan " + player.name + " tulosyhteeveto:\n")
                summary.append("\nArvioitu MMR: " + str(player.mmr) + ".\n")
                summary.append(player.name + " on " + player.rank_name + str(player.rank_number) + ".\n")
                summary.append("\nKokonaisvoittoprosentti: " + str('{:05.2f}'.format(player.winrate_calc(winrateResponse['win'],winrateResponse['lose']))) + "%.\n")
                summary.append("\nVoitti " + str(Matches.wins) + "/20 viime pelistään (" + str(Matches.current_winrate) + "%).\n")
                #summary.append("\nVoittoprosentti 20 viime pelin ajalta: " + str(Matches.current_winrate) + "%.\n")
                summary.append("\nViimeaikainen voittotrendi: " + str(round(Matches.current_winrate-player.winrate,3)) + "%.\n")

                #for row in summary:
                tulostus = ''.join(str(row) for row in summary)
                bot.reply_to(message, "{}".format(tulostus))
            except Exception as e:
                print("Statsdota exception:",e)
                bot.reply_to(message, "{}".format("Ei viimeaikaisia tuloksia pelaajalle " + keyword + "."))


    except ValueError as e:
        print(e)
        bot.reply_to(message, "{}".format("Please use the following format: '/statsdota andPlayerName'"))

@bot.message_handler(commands=['stats'], func=lambda msg: msg.text is not None)
def herostats(message):
    parts = message.text.split()
    try:
        partsLength = len(parts)
        keyword = ""

        # Komento annettu oikein
        keyword = str(parts[1])
        # Iso alkukirjain
        keyword = keyword.capitalize()
        if partsLength > 2:
            for q in range(1,partsLength - 1):
                keyword += " " + str(parts[q+1]).capitalize()

        # Hakutulokset
        searchResponse = requests.get('https://api.opendota.com/api/heroStats').json()
        heroData = ""
        heroFound = False
        for hero in searchResponse:
            if hero['localized_name'] == keyword:
                heroData = hero
                heroFound = True
        if heroFound == False:
            bot.reply_to(message, "{}".format("Ei tuloksia haulle " + keyword + "."))
        else:
            heroStats = list()
            heroStats.append("Heron " + heroData['localized_name'])
            heroStats.append("Immortal winrate: " + str(100* round(int(heroData['8_win'])/int(heroData['8_pick']),3))+ "%")
            #heroStats = list(map(heroStats.append, [heroData['localized_name'], round(int(heroData['8_pick'])/int(heroData['8_win']))]))

            tulostus = '\n'.join(str(row) for row in heroStats)
            # Tulostetaan käyttäjälle tulos
            bot.reply_to(message, "{}".format(tulostus))

            print(heroData)

    except ValueError as e:
        print(e)
        bot.reply_to(message, "{}".format("Please use the following format: '/stats andHeroName'"))


while True:
    try:
        bot.polling(none_stop=True, interval=0, timeout=10)
    except Exception as e:
        time.sleep(60)